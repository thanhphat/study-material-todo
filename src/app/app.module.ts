import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Forms module: using ngModel
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Using mat-button, input field
// Change md to mat from version 2
import { MatCardModule, MatToolbarModule, MatButtonModule, MatInputModule, MatCheckboxModule, MatListModule, MatIconModule } from '@angular/material';
// import { MatInputModule } from '@angular/material/input';

import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule, MatToolbarModule,
    MatButtonModule,
    MatInputModule, MatCheckboxModule, MatListModule, MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
